defmodule FollowTime.Limiter do
  use GenServer

  @moduledoc """
  Limits our request speed so that requests are only sent with a set minimum time between them.
  """

  def start_link(opts) do
    # State is: {is_timer_on, list_of_pids_and_callbacks}
    GenServer.start_link(__MODULE__, {false, []}, opts)
  end

  def init(args) do
    {:ok, args}
  end

  @doc """
  Send request to limiter.

  The request callback will be run when the limiter deems it suitable. After the call returns,
  wait_answer/1 should be called to wait for the answer.
  """
  def send_request(callback) when is_function(callback, 0) do
    GenServer.call(GenServer.whereis(__MODULE__), {:req, callback})
  end

  @doc """
  Wait for answer from limiter for the given time.
  """
  def wait_answer(time_to_wait) when is_integer(time_to_wait) do
    receive do
      {:limiter_answer, answer} -> answer
    after
      time_to_wait -> {:error, :timeout}
    end
  end

  @doc """
  Send request, then wait for answer.
  """
  def send_and_wait(callback, time_to_wait)
      when is_function(callback, 0) and is_integer(time_to_wait) do
    :ok = send_request(callback)
    wait_answer(time_to_wait)
  end

  def handle_call({:req, callback}, {from, _}, {false, []}) when is_function(callback, 0) do
    run_callback(from, callback)
    start_timer()
    {:reply, :ok, {true, []}}
  end

  def handle_call({:req, callback}, {from, _}, {true, callbacks}) when is_function(callback, 0) do
    {:reply, :ok, {true, callbacks ++ [{from, callback}]}}
  end

  # Stop if we ran out of requests
  def handle_info(:send_req, {_, []}) do
    {:noreply, {false, []}}
  end

  def handle_info(:send_req, {_, [{from, callback} | rest]}) do
    run_callback(from, callback)
    start_timer()
    {:noreply, {true, rest}}
  end

  defp start_timer() do
    Process.send_after(self(), :send_req, Env.fetch!(:follow_time, :min_request_interval))
  end

  defp run_callback(from, callback) do
    send(from, {:limiter_answer, callback.()})
  end
end
