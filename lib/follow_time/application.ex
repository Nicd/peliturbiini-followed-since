defmodule FollowTime.Application do
  # See https://hexdocs.pm/elixir/Application.html
  # for more information on OTP Applications
  @moduledoc false

  use Application

  def start(_type, _args) do
    # List all child processes to be supervised
    children = [
      Plug.Adapters.Cowboy.child_spec(
        :http,
        FollowTime.Router,
        [],
        port: Env.fetch!(:follow_time, :port) |> String.to_integer()
      ),
      FollowTime.Limiter.child_spec(name: FollowTime.Limiter)
    ]

    # See https://hexdocs.pm/elixir/Supervisor.html
    # for other strategies and supported options
    opts = [strategy: :one_for_one, name: FollowTime.Supervisor]
    Supervisor.start_link(children, opts)
  end
end
