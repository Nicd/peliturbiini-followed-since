defmodule FollowTime do
  import FollowTime.Limiter, only: [send_and_wait: 2]
  import Plug.Conn.Query, only: [encode: 1]
  require Logger

  # Time in milliseconds to wait for request being sent and answered
  @timeout 10000

  @doc """
  Get metadata of a single user.
  """
  def get_user_data(user) when is_binary(user) do
    callback = fn ->
      HTTPoison.get(
        "#{Env.fetch!(:follow_time, :api_user_url)}?#{encode(%{"login" => user})}",
        client_header()
      )
    end

    {:ok, %HTTPoison.Response{status_code: 200, body: body}} = send_and_wait(callback, @timeout)
    handle_user_data(body)
  end

  @doc """
  Get follow status of a user. Cursor is used to continue search later.
  """
  def get_follow_status(user_id, cursor \\ nil) when is_integer(user_id) do
    args = %{"from_id" => user_id}
    args = if is_binary(cursor), do: Map.put(args, "after", cursor), else: args

    callback = fn ->
      HTTPoison.get(
        "#{Env.fetch!(:follow_time, :api_follow_url)}?#{encode(args)}",
        client_header()
      )
    end

    {:ok, %HTTPoison.Response{status_code: 200, body: body}} = send_and_wait(callback, @timeout)
    handle_follow_data(body, user_id)
  end

  @doc """
  Get formatted follow time string for a user.
  """
  def get_follow_time(username) do
    with {:ok, %{"id" => id}} <- FollowTime.get_user_data(username),
         {:ok, since} <- FollowTime.get_follow_status(Integer.parse(id) |> elem(0)) do
      {:ok, since_iso, _} = DateTime.from_iso8601(since)
      now = DateTime.utc_now()
      diff = Timex.Interval.new(from: since_iso, until: now)

      Env.fetch!(:follow_time, :msgs)[:follow_time_msg]
      |> String.replace("{username}", username)
      |> String.replace("{channelname}", Env.fetch!(:follow_time, :channel_name))
      |> String.replace("{duration}", format_duration(diff))
    else
      {:error, :user_not_found} ->
        Env.fetch!(:follow_time, :msgs)[:user_not_found]
        |> String.replace("{username}", username)

      {:error, :does_not_follow} ->
        Env.fetch!(:follow_time, :msgs)[:follow_time_does_not_follow]
        |> String.replace("{username}", username)
        |> String.replace("{channelname}", Env.fetch!(:follow_time, :channel_name))
    end
  end

  defp client_header(), do: ["Client-ID": Env.fetch!(:follow_time, :client_id)]

  defp handle_user_data(body) when is_binary(body) do
    Logger.debug("Got user data: " <> body)

    case Poison.decode!(body) do
      %{"data" => [user_data]} ->
        {:ok, user_data}

      # This is Twitch's way of saying there is no such user: 200 OK and empty data ¯\_(ツ)_/¯
      %{"data" => []} ->
        {:error, :user_not_found}
    end
  end

  defp handle_follow_data(body, user_id) when is_binary(body) do
    Logger.debug("Got follow data: " <> body)
    body |> Poison.decode!() |> handle_follow_data(user_id)
  end

  # No more data was found -> no result
  defp handle_follow_data(%{"data" => []}, _) do
    {:error, :does_not_follow}
  end

  defp handle_follow_data(data, user_id) when is_map(data) do
    channel_id = Env.fetch!(:follow_time, :channel_id)

    value_finder = fn %{"to_id" => to_id, "followed_at" => followed_at} ->
      if to_id == channel_id do
        followed_at
      else
        nil
      end
    end

    case Enum.find_value(data["data"], value_finder) do
      nil ->
        # Need to ask for more data
        get_follow_status(user_id, data["pagination"]["cursor"])

      followed_since ->
        {:ok, followed_since}
    end
  end

  defp format_duration(diff) do
    units = Env.fetch!(:follow_time, :msgs)[:follow_time_units]

    units_list = [
      {:years, units.year},
      {:months, units.month},
      {:days, units.day},
      {:hours, units.hour},
      {:minutes, units.minute}
    ]

    Enum.reduce(units_list, {"", diff}, fn {unit, unit_str}, {str, cur_diff} ->
      dur = Timex.Interval.duration(cur_diff, unit)

      new_until = Timex.shift(cur_diff.until, [{unit, -dur}])
      new_diff = Timex.Interval.new(from: cur_diff.from, until: new_until)

      str = if dur > 0, do: str <> "#{dur}#{unit_str} ", else: str

      {str, new_diff}
    end)
    |> elem(0)
    |> String.trim_trailing()
  end
end
