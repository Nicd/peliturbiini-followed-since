defmodule FollowTime.Router do
  use Plug.Router
  require Logger

  plug Plug.Logger
  plug :match
  plug :dispatch

  get "/follow-time/:username" do
    try do
      send_resp(conn, 200, FollowTime.get_follow_time(username))
    rescue
      e ->
        Logger.error("Uncaught exception occurred:\n---\n#{Exception.format(:error, e)}")
        send_resp(conn, 200, "Error. :(")
    end
  end

  match _ do
    send_resp(conn, 404, "Invalid path.")
  end
end
