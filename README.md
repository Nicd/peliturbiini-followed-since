# Peliturbiini follow time finder

This little API can be used with a Twitch bot to get the time that a user has followed the channel. I made it for
[Peliturbiini](https://twitch.tv/peliturbiini) originally but it can be used for any channel.

## For dev

```
# Customise your settings by creating dev.secret.exs and prod.secret.exs as needed
$ mix compile
$ mix run --no-halt
```

## For deployment

```
$ MIX_ENV=prod COOKIE=secretcookie mix release --env=prod
```
