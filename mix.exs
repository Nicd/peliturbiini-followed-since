defmodule FollowTime.Mixfile do
  use Mix.Project

  def project do
    [
      app: :follow_time,
      version: "0.3.1",
      elixir: "~> 1.5",
      name: "Peliturbiini follow time API",
      start_permanent: Mix.env() == :prod,
      deps: deps()
    ]
  end

  # Run "mix help compile.app" to learn about applications.
  def application do
    [
      extra_applications: [:logger],
      mod: {FollowTime.Application, []},
      registered: [FollowTime.Limiter]
    ]
  end

  # Run "mix help deps" to learn about dependencies.
  defp deps do
    [
      {:plug, "~> 1.4.3"},
      {:cowboy, "~> 1.1.2"},
      {:httpoison, "~> 0.13.0"},
      {:poison, "~> 3.1.0"},
      {:timex, "~> 3.1.24"},
      {:distillery, "~> 1.5.2"},
      {:env, "~> 0.2.0"}
    ]
  end
end
