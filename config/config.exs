# This file is responsible for configuring your application
# and its dependencies with the aid of the Mix.Config module.
use Mix.Config

time_units = fn
  "en" ->
    %{
      year: "yr",
      month: "mo",
      day: "d",
      hour: "h",
      minute: "min"
    }

  "fi" ->
    %{
      year: "v",
      month: "kk",
      day: "pv",
      hour: "t",
      minute: "min"
    }
end

msgs = fn
  "en" ->
    %{
      # Returned output for follow time, the following things will be replaced:
      # {username}, {channelname}, {duration} (units configured below).
      follow_time_msg: "{username} has followed {channelname} for {duration}.",
      follow_time_does_not_follow: "{username} does not follow {channelname}. :(",
      follow_time_units: time_units.("en"),

      # When user is not found, you can use {username} here.
      user_not_found: "{username} is not a valid Twitch user."
    }

  "fi" ->
    %{
      follow_time_msg: "{username} on seurannut {channelname}a {duration}.",
      follow_time_does_not_follow: "{username} ei seuraa {channelname}a. :(",
      follow_time_units: time_units.("fi"),
      user_not_found: "Tuntematon käyttäjä {username}."
    }
end

# Override these values in your environment

lang = System.get_env("UI_LANG") || "en"

config :follow_time,
  # Port to bind to
  port: {:system, "PORT", "4123"},

  # URL to fetch user data from
  api_user_url: {:system, "API_USER_URL", "https://api.twitch.tv/helix/users"},

  # URL to fetch follows from
  api_follow_url: {:system, "API_FOLLOW_URL", "https://api.twitch.tv/helix/users/follows"},

  # Client ID to send to API
  client_id: {:system, "CLIENT_ID"},

  # Channel ID to fetch follow data for
  channel_id: {:system, "CHANNEL_ID"},
  channel_name: {:system, "CHANNEL_NAME"},

  # Min millisecond interval of requests to send
  min_request_interval: {:system, "MIN_REQ_INT", 1_000},
  msgs: msgs.(lang)
